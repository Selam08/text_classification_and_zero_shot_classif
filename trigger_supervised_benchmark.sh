#!/bin/bash
set -e

python3 train_eval_supervised_classif.py -mc bert-base-uncased -bs 64
#python3 train_eval_supervised_classif.py -mc bert-large-uncased 

#python3 train_eval_supervised_classif.py -mc robert-base 
#python3 train_eval_supervised_classif.py -mc robert-large -bs 16

#python3 train_eval_supervised_classif.py -mc microsoft/deberta-base