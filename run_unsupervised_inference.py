# general stuff
import argparse
import json

import torch
# base nlp
import transformers
from joblib import dump
from sklearn.decomposition import PCA
from sklearn.metrics import *
# clustering stuff
from sklearn.mixture import GaussianMixture  # , HDBSCAN
from sklearn.preprocessing import StandardScaler

# helpers
from src.utils import *

models_dependancies = {
    "bert":      {"tokenizer": transformers.BertTokenizer, "model": transformers.BertModel},
    "bart":      {"tokenizer": transformers.BartTokenizer, "model": transformers.BartModel},
    "deberta":   {"tokenizer": transformers.DebertaTokenizer, "model": transformers.DebertaModel},
    "debertav2": {"tokenizer": transformers.DebertaV2Tokenizer, "model": transformers.DebertaV2Model},
    "xlnet":     {"tokenizer": transformers.XLNetTokenizer, "model": transformers.XLNetModel}
}

metrics_list = [homogeneity_score, completeness_score, rand_score, mutual_info_score, fowlkes_mallows_score,
                v_measure_score, adjusted_mutual_info_score, normalized_mutual_info_score]


def main(model_name, model_config, data_path, batch_size, pca_dimensions, record_folder):
    # load data
    news_df = pd.read_json(data_path, lines=True)
    news_df["clean_headline"] = news_df.headline.apply(clean_text, apply_lem=False)

    # Initialize model
    tokenizer = models_dependancies[model_name]['tokenizer'].from_pretrained(model_config)
    model = models_dependancies[model_name]['model'].from_pretrained(model_config)
    model.cuda()
    print("=============== \n", model_name, "\n\t", model_config)

    # Project data in Model Latent space
    dataset_sequences = list(news_df.clean_headline.values)
    count = 0
    vectors = []
    for b in split_in_batches(dataset_sequences, batch_size=batch_size):
        with torch.no_grad():
            encoded_batch = tokenizer(b, padding=True, truncation=False, return_tensors="pt")
            encoding = {k: v.to('cuda:0') for k, v in encoded_batch.items()}
            embedded_batch = model(**encoding)
            vectors.append(
                get_average_vector(embedded_batch, encoding['input_ids'], pad_token=model.config.pad_token_id))
            if count < np.ceil(len(dataset_sequences) / batch_size) - 1:
                encoding.clear()
                del encoding, embedded_batch
            torch.cuda.empty_cache()
            count = count + 1
            print("batch count: ", count, end='\r')

    # Preprocessing of projected datapoints and save 
    print("=============== \n latent projection preprocessing")
    news_df["headline_vector"] = np.vsplit(torch.concatenate(vectors).cpu().numpy(), news_df.shape[0])
    X_raw = np.vstack(news_df.headline_vector)
    projection_name = "{0:s}_{1:s}.npy".format(model_name, model_config.replace("/", "_"))
    np.save(os.path.join(record_folder, "latent_spaces", projection_name), X_raw)
    X_scaled = StandardScaler().fit_transform(X_raw)
    X_reduced = PCA(n_components=pca_dimensions).fit_transform(X_scaled)

    # Clustering process
    print("=============== \n clustering grid search")
    metrics_path = os.path.join(record_folder, "metrics.csv")
    # min_cluster_size_ = 1000
    for N_components in [50, 45, 42, 38, 30, 20]:
        for x_type in ["scaled", "raw"]:
            if x_type == "scaled":
                x = X_scaled
            elif x_type == "raw":
                x = X_raw
            else:
                raise ValueError('x_type should be sclaed or raw')

            # Initialize and trigger clustering
            clustering = GaussianMixture(n_components=N_components, covariance_type='diag', random_state=0).fit(x)
            # clustering = HDBSCAN(min_cluster_size=min_cluster_size_, n_jobs=-1).fit(x)
            news_df["predicted_category"] = clustering.predict(x)

            # Define record parameters
            exp_name = "{3:s}_x_type_{0:s}_clust_{1:s}_ncomps_{2:d}".format(
                x_type, clustering.__class__.__name__,
                N_components, model.config.name_or_path.split('/')[-1]
            )
            exp_path = os.path.join(record_folder, exp_name)

            # Save clustering model
            dump(clustering, exp_path + '.joblib')

            # Records predictions
            news_df[["headline", "category", "predicted_category"]].to_csv(exp_path + ".csv")
            with open(exp_path + ".json", 'w') as fp:
                json.dump(clustering.get_params(), fp)

            # Update metrics csv file
            metric_df = pd.DataFrame({'setup': [exp_name]})
            for fun in metrics_list:
                metric_df[fun.__name__] = [fun(news_df.category.values, news_df.predicted_category.values)]
            if os.path.isfile(metrics_path):
                metrics_df = pd.concat([pd.read_csv(metrics_path), metric_df], ignore_index=True)
            else:
                metrics_df = metric_df
            metrics_df.to_csv(metrics_path, index=False)

    return


# main routine
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Run unsupervised classification inference')
    parser.add_argument('-mn', '--model_name', help='Name of the specific model architecture to train',
                        default="bert-base-uncased", type=str)
    parser.add_argument('-mc', '--model_config', help='Name of the pretrained config to load',
                        default="bert-base-uncased'", type=str)
    parser.add_argument('-d', '--data_path', help='folder to containing data to use', default="data/news.jsonl",
                        type=str)
    parser.add_argument('-rf', '--record_folder', help='folder to containing data to use', default="results/ex1b",
                        type=str)
    parser.add_argument('-bs', '--batch_size', help='Size of batch for inference (depend on VRAM)', default=256,
                        type=int)
    parser.add_argument('--pca_dimensions', help='Number of PCA components to keep', default=40, type=int)

    args = parser.parse_args()
    main(**vars(args))
