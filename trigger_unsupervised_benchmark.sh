#!/bin/bash
set -e

python3 run_unsupervised_classif.py -mn bart -mc facebook/bart-base
python3 run_unsupervised_classif.py -mn bart -mc facebook/bart-large-cnn -bs 128

python3 run_unsupervised_classif.py -mn bert -mc bert-base-uncased
python3 run_unsupervised_classif.py -mn bert -mc bert-large-uncased

python3 run_unsupervised_classif.py -mn deberta -mc microsoft/deberta-base

python3 run_unsupervised_classif.py -mn debertav2 -mc microsoft/deberta-v2-xlarge -bs 128
python3 run_unsupervised_classif.py -mn debertav2 -mc microsoft/deberta-v3-base

python3 run_unsupervised_classif.py -mn xlnet -mc xlnet-base-cased