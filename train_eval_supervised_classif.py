import argparse

from transformers import AutoTokenizer
from transformers import EarlyStoppingCallback, IntervalStrategy
from transformers import TrainingArguments, Trainer

from src.multilabel_model import MultiLabelModel
from src.seq2labels_datasets import Seq2LabelDataset
from src.utils import *

os.environ["TOKENIZERS_PARALLELISM"] = "false"


def main(model_config, record_dir, data_path, batch_size, n_patience, n_epochs):
    # Load data
    news_df = pd.read_json(data_path, lines=True)

    # Clean and format data
    news_df["clean_headline"] = news_df.headline.apply(clean_text)
    news_df = news_df.loc[news_df.clean_headline != '']
    news_df = news_df[['clean_headline', 'category']]
    labels = news_df.category.unique()
    id2label = {idx: label for idx, label in enumerate(labels)}
    label2id = {label: idx for idx, label in enumerate(labels)}
    news_df['labels_array'] = news_df.category.apply(
        lambda x: np.array([1.0 if label == x else 0.0 for label in labels])
    )

    # Split data in train, validation and test set
    train, val, test = get_stratified_split(news_df, 0.8, 0.1, news_df.category.value_counts().to_dict(), shuffle=True,
                                            random_state=0)

    # Preprocess data
    tokenizer = AutoTokenizer.from_pretrained(model_config)
    train_token = tokenizer(train.clean_headline.tolist(), padding=True, truncation=True, max_length=128,
                            return_tensors='pt')
    val_token = tokenizer(val.clean_headline.tolist(), padding=True, truncation=True, max_length=128,
                          return_tensors='pt')
    train_dataset = Seq2LabelDataset(train_token, train.labels_array.tolist())
    val_dataset = Seq2LabelDataset(val_token, val.labels_array.tolist())

    # Initialize Model
    model = MultiLabelModel(
        model_config,
        num_labels=len(labels),
        problem_type="multi_label_classification",  # this is important
        id2label=id2label,
        label2id=label2id
    )
    model_dir = model.init_record_forler(record_dir)

    # Define Trainer
    trainer_args = TrainingArguments(
        # learning_rate=2e-5,
        # weight_decay=0.01,
        output_dir=model_dir,
        evaluation_strategy=IntervalStrategy.STEPS,  # "steps",
        eval_steps=np.ceil(np.ceil(len(train_dataset) / batch_size) / n_patience),
        save_steps=np.ceil(np.ceil(len(train_dataset) / batch_size) / n_patience),
        per_device_train_batch_size=batch_size,
        per_device_eval_batch_size=batch_size,
        num_train_epochs=n_epochs,
        seed=0,
        load_best_model_at_end=True,
        metric_for_best_model='loss',
        greater_is_better=False,
        save_total_limit=2,
        optim="adamw_torch"
    )
    trainer = Trainer(
        model=model.inner_model,
        args=trainer_args,
        train_dataset=train_dataset,
        eval_dataset=val_dataset,
        compute_metrics=compute_metrics,
        callbacks=[EarlyStoppingCallback(early_stopping_patience=n_patience)],
    )
    model.records_info(trainer.args.to_dict(), 'trainer_info.json')

    # Train
    trainer.train()
    del train_token, train_dataset

    # Eval model
    test_token = tokenizer(test.clean_headline.tolist(), padding=True, truncation=True, max_length=128,
                           return_tensors='pt')
    test_dataset = Seq2LabelDataset(test_token, test.labels_array.tolist())
    model.eval_testset(test_dataset)
    return


# main routine
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Run unsupervised classification inference')

    parser.add_argument('-mc', '--model_config', help='Name of the pretrained config to load',
                        default="bert-base-uncased'", type=str)
    parser.add_argument('-d', '--data_path', help='folder to containing data to use', default="data/news.jsonl",
                        type=str)
    parser.add_argument('-rd', '--record_dir', help='folder to containing data to use', default="results/ex1a/models",
                        type=str)
    parser.add_argument('-bs', '--batch_size', help='Size of batch for inference (depend on VRAM)', default=32,
                        type=int)
    parser.add_argument('-np', '--n_patience', help='Patience for Early Stop Callback', default=8, type=int)
    parser.add_argument('-ne', '--n_epochs', help='Maximum number of epochs to train', default=10., type=float)

    args = parser.parse_args()
    main(**vars(args))
