ARG BASE_IMAGE="huggingface/transformers-pytorch-gpu:4.29.2"
FROM ${BASE_IMAGE}


#USER root
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
#    apt-utils \
    vim

# install the python dependencies
COPY requirements.txt /tmp/
RUN pip install -r /tmp/requirements.txt

# Jupyter setting
#RUN jupyter nbextension enable --py widgetsnbextension

# Download nltk data
RUN python3 -m nltk.downloader all

# Download spacy model
RUN python3 -m spacy download en_core_web_lg
