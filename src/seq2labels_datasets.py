import torch
from torch.utils.data import Dataset


# Create torch dataset
class Seq2LabelDataset(Dataset):
    """
    Custom class of torch.utils.data.dataset for multi-label classification dataset
    """

    def __init__(self, encodings, labels=None):
        self.encodings = encodings
        self.labels = labels

    def __getitem__(self, idx):
        item = {key: val[idx].clone() for key, val in self.encodings.items()}
        if self.labels:
            item["labels"] = torch.tensor(self.labels[idx])
        return item

    def __len__(self):
        return len(self.encodings["input_ids"])

    # def to_dataloader(self, batch_size: int = 128, **kwargs) -> DataLoader:
    #    return DataLoader(self,**kwargs)
