import os

import matplotlib.pyplot as plt
import nltk
import numpy as np
import pandas as pd
import torch
from sklearn.metrics import *
from transformers import EvalPrediction


def clean_text(text, stop_words=[], apply_lem=False):
    """
    Clean text by applying lemmatisation and removing stop_words

    Args:
        text (str): text to process
        stop_words (List): list of stop words to remove
        apply_lem (bool): if True, apply lemmatisation

    Returns:
        (str): clean text
    """
    cleaned_text = text.lower().strip().split()

    if stop_words:
        cleaned_text = [w for w in cleaned_text if w not in stop_words]

    # Lemmatisation
    if apply_lem:
        lem = nltk.stem.wordnet.WordNetLemmatizer()
        cleaned_text = [lem.lemmatize(w) for w in cleaned_text]

    return " ".join(cleaned_text)


def w2v_average_vector(text, model, apply_mean=True, verbose=False):
    """
    project words of a text (if available in model vocab).
    compute average words vectors
    Args:
        text (str): text to vectorized
        model (gensim.model): word 2 vec model to use
        apply_mean (bool): if True, average words vector
        verbose (bool): if True, print unavailable vocab

    Returns:
        (numpy array): word vector
    """
    vectors = []
    for w in text:
        try:
            next_vec = model[w]
            vectors.append(next_vec)
        except KeyError:
            if verbose:
                print('Unknown key: ', w)
    if apply_mean:
        return np.asarray(vectors).mean(axis=0)
    else:
        return np.asarray(vectors)


def encode_sentence(sentence, model, tokenizer):
    """
    encode text using tokenizer and huggingface model
    Args:
        sentence (str): text to encode
        model (transformer model): transformer model to use
        tokenizer (transformer tokenizer): tokenizer to use

    Returns:

    """
    token = tokenizer.encode(sentence, return_tensors="pt")
    with torch.no_grad():
        embbeding = model(token)
    del token
    return embbeding[0][0].mean(axis=0)


def split_in_batches(iterable_obj, batch_size=128):
    """
    Split iterable object in batches

    Args:
        iterable_obj (List, array, tensor...): object to split
        batch_size (int): batch size
    Returns:
         (generator)
    """
    N_obj = len(iterable_obj)
    for i in range(0, N_obj, batch_size):
        yield iterable_obj[i:min(i + batch_size, N_obj)]


def get_average_vector(embedded_batch, tokens_tensor, pad_token=0):
    """
    Average sequence vector, resulting from tranfsormer model, ignoring padding tokens

    Args:
        embedded_batch (tensor): encoded text
        tokens_tensor (tensor): tokenized text
        pad_token (int): token corresponding to padding

    Returns:
        (tensor): average vector
    """
    padded_token = 1. * (tokens_tensor != pad_token)
    embedded_batch_sums = torch.mul(embedded_batch[0], padded_token[:, :, None]).sum(axis=1)
    average_vectors = torch.div(embedded_batch_sums, padded_token.sum(axis=1)[:, None])
    return average_vectors


def plot_confusion_matrix(y_true, y_pred, labels, figsize=(25, 25), cm=[]):
    """Generate chart of confusion matrix
        Args:
            y_true: (array_like) true target value
            y_pred: (array_like) predicted value
            labels: (List) list of class names
            figsize:(tuple) dimension of figue
            cm:     (List) confusion matrix scores from different fold to average
        Returns:
            fig: (matplotlib.pyplot.figue)
            ax: (matplotlib.pyplot.axes)"""
    fig, ax = plt.subplots(figsize=figsize)
    if cm:
        cm = np.array(cm).mean(axis=0)
    else:
        cm = confusion_matrix(y_true=y_true, y_pred=y_pred, normalize="true")
    disp = ConfusionMatrixDisplay(confusion_matrix=cm,
                                  display_labels=labels)
    disp.plot(cmap=plt.cm.Blues, ax=ax, colorbar=False, xticks_rotation="vertical", values_format="0.2f")
    return fig, ax


def get_ypred_ytrue(inference, dataset_df, missing_labels=[]):
    """Process inference data and original dataset to compute best recommended class per sequences (y_pred),
    true class (y_true),mappings of class name to numerical class encoding (encod2lab, lab2encod) and result as
     pandas DataFrame
        Args:
            inference: (List(tuple())) output of predict method from fast-bert models
            dataset_df: (pandas DataFrame) original test dataset, used for inference
            missing_labels: (List) list of label to omit (missing from test dataset)
        Returns:
            y_pred: (pandas Serie)
            y_true: (pandas Serie)
            encod2lab: (dic)
            lab2encod: (dic)
            reslab : (List) list of unique labels which have been infered
            result_df : (pandas DataFrame)
            """
    result_df = pd.DataFrame([dict(t) for t in inference])
    reslab = [lab for lab in result_df.columns.to_list() if not lab in missing_labels]
    encod2lab = {i + 1: l for i, l in enumerate(reslab)}
    lab2encod = {v: k for k, v in encod2lab.items()}
    y_pred = result_df.idxmax(axis=1).apply(lambda x: lab2encod[x])
    y_true = dataset_df.idxmax(axis=1).apply(lambda x: lab2encod[x])
    return y_pred, y_true, encod2lab, lab2encod, reslab, result_df


def benchmarks(y_true, y_pred, test_df, result_df, labels, record_folder):
    """Compute berchmarking metrics (Confusion matrix, Percentage of correct set vs number of class per set, metric
        report)
        Args:
            y_true: (array_like) true target value
            y_pred: (array_like) predicted value
            test_df: (pandas DataFrame) original test dataset, used for inference
            result_df: (pandas DataFrame) prediction data
            labels: (List) list of class names
            record_folder (str) path location to record charts and report
        Returns:
            None
            """
    # Confusion matrix
    fig_cm, ax_cm = plot_confusion_matrix(y_true, y_pred, labels)
    fig_cm.savefig(os.path.join(record_folder, "confusion_matrix.png"), dpi=200, bbox_inches="tight")

    # Compute usage score
    true_df = test_df.idxmax(axis=1)
    ordering = result_df.apply(lambda row: row.sort_values(ascending=False).keys().to_list(), axis=1)
    assert all(true_df.index == ordering.index)
    score_per_n_best = {n_best: np.sum([true_df[ind] in ordering[ind][:n_best] for ind in range(true_df.shape[0])])
                        for n_best in range(1, 50)}
    score_per_n_best_perc = {k: 100 * v / true_df.shape[0] for k, v in score_per_n_best.items()}

    # Plot usage graph
    fig, ax = plt.subplots(2, 1, figsize=(15, 10))
    plot_journalIn_vs_N_selected(ax[0], score_per_n_best, score_per_n_best_perc)
    plot_journalIn_vs_N_selected(ax[1], score_per_n_best, score_per_n_best_perc, max_x=15)
    ax[0].set_title("Number of proposal for at least 95%: {0:d}\nNumber of proposal for at least 90%: {1:d}".format(
        get_closest_index(95, score_per_n_best_perc), get_closest_index(90, score_per_n_best_perc)))
    ax[1].set_title(
        "Correct set with 5 best proposal selected: {0:.0f}%\nCorrect set with 3 best proposal selected: {1:.0f}%".format(
            score_per_n_best_perc[5], score_per_n_best_perc[3]))
    ax[0].set_xlabel('')
    fig.savefig(os.path.join(record_folder, "Usage.png"), dpi=200, bbox_inches="tight")

    # compute report
    df_report = get_report(y_true, y_pred, labels)
    df_report.to_csv(os.path.join(record_folder, "report.csv"))


def plot_classe_in_vs_n_selected(score_per_n_best_perc, max_x=None, ax=None):
    """Produce chart showing the percetage of correct (containing true class) selected set of classes depending on the
        number of best match in the set
        Args:
            ax: (matplotlib axis)
            score_per_n_best_perc: (dic) containing percentages with number of journal in set as key
            max_x: (int) maximum x value to clipp the chart
        Return:
            ax: (matplotlib axis)
            """
    if not ax:
        _, ax = fig, ax = plt.subplots(figsize=(7, 10))
    ax.fill_between(x=list(score_per_n_best_perc.keys()), y1=list(score_per_n_best_perc.values()),
                    y2=[100 for k in score_per_n_best_perc.keys()], alpha=0.5, color="r")
    ax.fill_between(x=list(score_per_n_best_perc.keys()), y1=[0 for k in score_per_n_best_perc.keys()],
                    y2=list(score_per_n_best_perc.values()), alpha=0.5)
    ax.set_ylabel("% of papers in the selection")
    ax.set_xlabel("Number of best categories selected")
    ax.xaxis.set_ticks(np.arange(1, max(score_per_n_best_perc.keys()) + 1, 1))
    ax.grid("on", color="k", alpha=0.2)
    ax.set_ylim(0, 100)
    if isinstance(max_x, type(None)):
        ax.set_xlim(1, max(score_per_n_best_perc.keys()))
    else:
        ax.set_xlim(1, max_x)
    return ax


def get_closest_index(target_value, perc_dic):
    """get key corresponding to the value which is the closest to the target
    Args:
        target_value: (float)
        perc_dic: (dic)"""
    dif_dic = {k: abs(target_value - v) for k, v in perc_dic.items()}
    return min(dif_dic, key=dif_dic.get)


def get_report(y_true, y_pred, labels):
    """Generate performance report as pandas DataFrame
        Args:
            y_true: (array_like) true target value
            y_pred: (array_like) predicted value
            labels: (list) class names
        Return:
            class_report: (pandas.DataFrame)"""
    class_report = classification_report(y_true=y_true, y_pred=y_pred, target_names=labels, output_dict=True)
    class_report = pd.DataFrame(class_report).T
    return class_report


def get_stratified_split(df, ratio_tr, ratio_vl, group_count, group_col="category", shuffle=True, random_state=None):
    """
    Create stratified split of dataframe into train, val and test based on given ratio
    Args:
        df (pd dataframe): full dataset
        ratio_tr (float): train ratio
        ratio_vl (float): validation ratio
        group_count (pd series): stratification
        group_col (str): column name of the group
        shuffle (bool): if True, shuffle split datasets
        random_state (int): random state

    Returns:
        (List(pd dataframe))
    """
    train, val, test = [], [], []
    if shuffle:
        df = df.sample(frac=1, random_state=random_state)
    grp = [g for g in df.groupby(group_col)]
    for g_name, g_df in grp:
        train_split, val_split = int(group_count[g_name] * ratio_tr), max(1, int(group_count[g_name] * ratio_vl))
        if (train_split + val_split) == group_count[g_name]:
            train_split -= 1
        train.append(g_df.iloc[:train_split])
        val.append(g_df.iloc[train_split:train_split + val_split])
        test.append(g_df.iloc[train_split + val_split:])
    return [pd.concat(d).sample(frac=1, random_state=random_state) for d in [train, val, test]]


# source: https://github.com/NielsRogge/Transformers-Tutorials/tree/master/BERT
def multi_label_metrics(predictions, labels, threshold=0.75):
    """
    metric computation for multi-label classification case
    Args:
        predictions (tensor): prediction before sigmoid
        labels (List): labels name
        threshold (float): threshold probability to match predicted classes

    Returns:
        metrics (dic): dictionnary of computed metrics
    """
    # first, apply sigmoid on predictions which are of shape (batch_size, num_labels)
    sigmoid = torch.nn.Sigmoid()
    probs = sigmoid(torch.Tensor(predictions))
    # next, use threshold to turn them into integer predictions
    y_pred = np.zeros(probs.shape)
    y_pred[np.where(probs >= threshold)] = 1
    # finally, compute metrics
    y_true = labels
    f1_micro_average = f1_score(y_true=y_true, y_pred=y_pred, average='micro')
    roc_auc = roc_auc_score(y_true, y_pred, average='micro')
    accuracy = accuracy_score(y_true, y_pred)
    # return as dictionary
    metrics = {'f1':       f1_micro_average,
               'roc_auc':  roc_auc,
               'accuracy': accuracy}
    return metrics


def compute_metrics(p: EvalPrediction):
    """
    Args:
        p ():

    Returns:

    """
    preds = p.predictions[0] if isinstance(p.predictions, tuple) else p.predictions
    result = multi_label_metrics(predictions=preds, labels=p.label_ids)
    return result


def sample_data(select_classes, full_data, n_samples=3000, class_colonm='category'):
    """
    filter dataframe dataset based on a list of classes and sample
    Args:
        select_classes (List(str)): list of classes name to select
        full_data (pd dataframe): data to process
        n_samples (int): number of sample to extract
        class_colonm (str): column name of the classe in the dataframe

    Returns:

    """
    sample_df = full_data.groupby(class_colonm, group_keys=False).apply(lambda x: x.sample(min(x.shape[0], n_samples)))
    sample_df = sample_df.loc[sample_df[class_colonm].isin(select_classes)].reset_index(drop=True)
    return sample_df
