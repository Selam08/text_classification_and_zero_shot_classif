import datetime
import json

import torch
from sklearn.metrics import classification_report
from torch.nn import Sigmoid
from transformers import AutoModelForSequenceClassification, Trainer

from .utils import *


class MultiLabelModel(AutoModelForSequenceClassification):
    """
    Custom class with extra method to run inference and evaluate test set prediction
    """

    def __init__(self, *args, **kwargs):
        # super().__init__(*args, **kwargs)
        self.inner_model = super().from_pretrained(*args, **kwargs)
        self.record_folder = None

    def init_record_forler(self, path, log_params=True):
        """
        Initialize record folder. Check if path exists and create if needed.
        Args:
            path (str): path to record file
            log_params (bool): if True, record config info as json

        Returns:

        """
        folder_name = self.inner_model.config.name_or_path
        run_start_time = datetime.datetime.today().strftime('%Y-%m-%d_%H-%M-%S')
        self.record_folder = os.path.join(path, folder_name, run_start_time)
        os.makedirs(self.record_folder, exist_ok=True)
        if log_params:
            with open(os.path.join(self.record_folder, "config_info.json"), "w") as outfile:
                json.dump(self.inner_model.config.to_dict(), outfile)

        return self.record_folder

    def records_info(self, info_dict, file_name):
        """
        record a dictionary of information as json in the record folder
        Args:
            info_dict (dict): info to record
            file_name (str): filename of the json
        """
        if not self.record_folder:
            raise ValueError('Record folder need to be define !')
        with open(os.path.join(self.record_folder, file_name), "w") as outfile:
            json.dump(info_dict, outfile)

    def predict(self, text, tokenizer, device='cuda:0'):
        """
        run inference on a sequence of text and return the best
        matches ids and labesl
        Args:
            text (str):  sequence of text to predict
            tokenizer (transformer.Tokenizer): tokeniszer to use
            device (str): cuda or cpu

        Returns:
            predicted_ids
            predicted_labels
        """
        encoding = tokenizer(text, return_tensors="pt")
        encoding = {k: v.to(device) for k, v in encoding.items()}
        outputs = self.inner_model(**encoding)
        sigmoid = Sigmoid()
        probs_array = sigmoid(outputs.logits.squeeze().cpu())
        predicted_ids = np.argsort(probs_array, axis=1)[:, -1]
        predicted_labels = [id2label[idx] for idx in predicted_ids]
        return predicted_ids, predicted_labels

    def infer_testset(self, test_dataset):
        """
        run inference no a dataset object
        Args:
            test_dataset (torch.util.dataset): dataset to infer

        Returns:
            probs_array (np.array) : array (size n_sample x N_labels) of predicted probability
            ordered_ids (numpy.array) : array of ids with decreasing probability
            ordered_labels (numpy.array) : array of labels with decreasing probability
            best_match (numpy.array) : array with one best match id per sample
        """
        predictions_outputs = Trainer(self.inner_model).predict(test_dataset)
        raw_pred = predictions_outputs.predictions
        sigmoid = torch.nn.Sigmoid()
        probs_array = sigmoid(torch.from_numpy(raw_pred)).numpy()
        ordered_ids = np.argsort(probs_array, axis=1)[:, ::-1]
        best_match = ordered_ids[:, 0]
        ordered_labels = [[self.inner_model.config.id2label[i_order] for i_order in order_row] for order_row in
                          ordered_ids]

        return probs_array, ordered_ids, ordered_labels, best_match

    def eval_testset(self, test_dataset):
        """
        run inference on a dataset containing true labels.
        Produce Usage Charts, confusion matrix and report.
        Records all in the model folder.
        Args:
            test_dataset (torch.util.dataset): dataset to test
        """
        # Inference and format output
        probs_array, ordered_ids, ordered_labels, best_match = self.infer_testset(test_dataset)
        true_label_array = test_dataset[:]['labels']
        true_labels = (true_label_array @ np.arange(true_label_array.shape[1])).int().numpy()
        score_per_n_best = {
            n_best: np.sum([true_labels[ind] in ordered_ids[ind][:n_best] for ind in range(true_labels.shape[0])])
            for n_best in range(1, true_label_array.shape[1])}
        score_per_n_best_perc = {k: 100 * v / true_labels.shape[0] for k, v in score_per_n_best.items()}

        # Usage chart
        fig, ax = plt.subplots(2, 1, figsize=(15, 10))
        plot_classe_in_vs_n_selected(score_per_n_best_perc, ax=ax[0])
        plot_classe_in_vs_n_selected(score_per_n_best_perc, max_x=15, ax=ax[1])
        ax[0].set_title("Number of proposal for at least 95%: {0:d}\nNumber of proposal for at least 90%: {1:d}".format(
            get_closest_index(95, score_per_n_best_perc), get_closest_index(90, score_per_n_best_perc)))
        ax[1].set_title(
            "Correct set with 5 best proposal selected: {0:.0f}%\nCorrect set with 3 best proposal selected: {1:.0f}%".format(
                score_per_n_best_perc[5], score_per_n_best_perc[3]))
        ax[0].set_xlabel('')
        if self.record_folder:
            fig.savefig(os.path.join(self.record_folder, "Usage.png"), dpi=200, bbox_inches="tight")

        # Confusion Matrix
        list_labels = [self.inner_model.config.id2label[i] for i in range(len(self.inner_model.config.id2label))]
        fig_cm, ax_cm = plot_confusion_matrix(true_labels, best_match, list_labels)
        if self.record_folder:
            fig_cm.savefig(os.path.join(self.record_folder, "confusion_matrix.png"), dpi=200, bbox_inches="tight")

        # Report
        class_report = classification_report(y_true=true_labels, y_pred=best_match, target_names=list_labels,
                                             output_dict=True)
        class_report = pd.DataFrame(class_report).T
        if self.record_folder:
            class_report.to_csv(os.path.join(self.record_folder, "report.csv"))
